import os
import git
from git import Repo
from git import BlobFilter


# Verificar si hay cambios sin guardar
def valoidar_commits(repo:Repo) -> bool:
    # if repo.is_dirty(untracked_files=True):
    #     proyecto = f'{proyecto} '
    #     print('Hay cambios sin guardar en el repositorio')
    return repo.is_dirty(untracked_files=True)


# Verificar si hay ramas sin sincronizar
def validar_sincronizacion(repo:Repo) -> list[str] :
    remote_branchs = repo.remotes.origin.fetch()
    local_branchs =  dict(map(lambda x: (x.name,x), repo.branches) )

    response = []
    for remote in remote_branchs:
        branch = remote.name.split('/')[1]

        if branch in local_branchs.keys():
            local = local_branchs.get(branch)
            staus =f'{branch}'
            
            if local.commit != remote.commit:
                staus = f'󱓍 {staus}'
            else:
                staus = f'󱓏 {staus}'
            
            if valoidar_commits(local.repo):
                staus = f'{staus} '
            
            response.append(staus)
    
    return response
            

    # for remote_branch in repo.remotes.origin.fetch():
    #     print(f'{remote_branch.name} {repo.branches}')
    #     if remote_branch.name in repo.branches:
    #         local_branch = repo.branches[remote_branch.name]
    #         if local_branch.commit != remote_branch.commit:
    #             proyecto = f'{proyecto} '
    #             print(f'Rama {local_branch.name} no sincronizada')



def check_projects(directory):

    for root, dirs, files in os.walk(directory):
        if '.git' in dirs:
            repo_path = os.path.join(root, '.git')
            if os.path.isdir(repo_path):
                proyecto = f' {root}'
                try:
                    repo = Repo(repo_path)
                    res = validar_sincronizacion(repo)

                    proyecto = f'{proyecto} \n\t {" | ".join(res) }'
                    
                except Exception as e:
                    print(f'Error al revisar el repositorio {repo_path}: {e}')
                
                print(f'{proyecto} \n')

if __name__ == "__main__":
    # directory = input("Ingrese la ruta del directorio a revisar: ")
    directory = "/home/tdtxle/Developer"
    check_projects(directory)


# import os
# from git import Repo

# # Ruta al directorio principal de tus proyectos
# ruta_proyectos = '/home/tdtxle/Developer'

# def revisar_proyectos(ruta_proyectos):
#     for root, dirs, files in os.walk(ruta_proyectos):
#         ruta_proyecto = os.path.join(ruta_proyectos, proyecto)
#         if os.path.isdir(ruta_proyecto):
#             print(f'Revisando proyecto: {proyecto}')
#             try:
#                 repo = Repo(ruta_proyecto)
#                 # Verificar si hay ramas sin sincronizar
#                 for remote_branch in repo.remotes.origin.fetch():
#                     if remote_branch.name in repo.branches:
#                         local_branch = repo.branches[remote_branch.name]
#                         if local_branch.commit != remote_branch.commit:
#                             print(f'Rama {local_branch.name} no sincronizada')
#                 # Verificar si hay cambios sin guardar
#                 if repo.is_dirty(untracked_files=True):
#                     print('Hay cambios sin guardar en el repositorio')
#             except Exception as e:
#                 print(f'Error al revisar el repositorio {proyecto}: {e}')

# if __name__ == "__main__":
#     revisar_proyectos(ruta_proyectos)
