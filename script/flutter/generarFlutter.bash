## obtener version actual
CURRENT_DATE=$(date +"%y.%m.%d")
CURRENT_TAG_NAME=$(git tag --list | sort -V | grep -oc "$CURRENT_DATE")
NEW_TAG=$([ $CURRENT_TAG_NAME = 0 ] && echo "$CURRENT_DATE" || echo "$CURRENT_DATE+$CURRENT_TAG_NAME")
V=$(echo "$NEW_TAG" | tr . -)

## cambiar version pubspec
OLD_VERCION=$(cat pubspec.yaml | grep "version: ")
sed -i -e "s/$OLD_VERCION/version: $NEW_TAG/g" pubspec.yaml

##crear apk
flutter build apk --release
CURRENT_LOCATION=$( pwd; )
APK="$CURRENT_LOCATION/build/app/outputs/apk/release"
mv "$APK/app-release.apk" "$APK/cliente-$V.apk"


# while getopts u:a:f: flag
# do
#     case "${flag}" in
#         # u) username=${OPTARG};;
#         # a) age=${OPTARG};;
#         # f) fullname=${OPTARG};;
#         u) username="pak";;
#         a) age=23;;
#         f) fullname="adasd ads";;
#     esac
# done
# echo "Username: $username";
# echo "Age: $age";
# echo "Full Name: $fullname";