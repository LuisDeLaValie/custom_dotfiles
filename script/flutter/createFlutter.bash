#!/bin/bash

flutter create $*

total_args=$#
nombre_proyecto="${!total_args}"
 
cd $nombre_proyecto
mkdir -p lib/{data/{models,providers,repositories},router,screens/home,utils,widgets}

touch lib/screens/home/home_screen.dart
touch lib/screens/home/home_view.dart

