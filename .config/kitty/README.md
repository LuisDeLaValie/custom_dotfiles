# KITTY

Para configurar la terminal *kitty*, copiamos el directorio actual en `~/.config/kitty`:

### Configuración
Puedes configurar la terminal kitty de dos maneras: haciendo una copia de este directorio o creando un enlace simbólico.


**Copia del directorio**
```sh 
cd ..
cp -r kitty ~/.config

```
**Enlace Simbólico**
```sh
ln -s $(pwd) ~/.config/kitty
```
>   En caso de que aparezca un mensaje indicando que el enlace o el directorio ya existen, puedes eliminar la configuración actual con el comando `rm -r ~/.config/kitty`. Después podrás ejecutar el comando nuevamente.





### Actualizar

```sh

curl -LOJ https://github.com/dracula/kitty/archive/master.zip
unzip kitty-master.zip
cp kitty-master/{dracula.conf,diff.conf} .
rm -r kitty-master kitty-master.zip
echo "include dracula.conf" >> kitty.conf

```