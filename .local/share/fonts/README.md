# Fuentes

Para configurar las fuentes, es necesario reemplazar `/usr/share/fonts` con el contenido de esta carpeta. Puedes hacerlo fácilmente copiando el contenido de este directorio o reemplazando la carpeta completa.

### Reemplazar directorio
Antes de reemplazar el directorio, asegúrate de copiar todos los archivos que ya existen:

```sh
cp -r /usr/share/fonts/* .
```

Esto copiará todo el contenido de `/usr/share/fonts` a este directorio.

Ahora eliminamos el directorio `/usr/share/fonts` actual:
```sh
rm -rf /usr/share/fonts
```

ya que lo eliminaro podremos hacer una copia de este director o crear un enlace de este direcorio   

**Mover direcorio**
```sh 
cd ..
mv fonts /usr/share/fonts/

```
**Enlace Simbólico**
```sh
ln -s $(pwd)/fonts /usr/share/fonts
```
>   `$(pwd)/fonts` es la ruta completa de este directorio. Puedes conocerla usando el comando `pwd`.

